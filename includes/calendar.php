<?php $days = data_days();?>
<div class="calendar">
    <div class="av-grid row v1">
        <?php foreach($days as $day) :
           $color = $day['color'];
        ?>
                <div
                    class="col-xs-4 col-sm-2 square-widget" style="background-color:<?php echo $color; ?>;"
                    data-day="<?php echo $day['id']; ?>"
                    data-display="<?php echo allow_day($day['id']) ? $day['display'] : 'none'; ?>"
                    data-type="<?php echo $day['type']; ?>"
                    <?php if(isset($day['src'])): ?>
                        data-src=<?php echo $day['src']; ?>
                    <?php endif; ?>
                >
                    <img src="<?php echo get_img('1x1.png') ?>" alt="placeholder">
                    <div class="content">
                        <div class="valign">
                            <div class="valign-content">
                                <?php echo $day['id']; ?>
                            </div>
                        </div>
                    </div>
                </div>
        <?php endforeach; ?>
    </div>
</div>