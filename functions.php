<?php
date_default_timezone_set('Europe/Paris');
    function versions_nav(){
        global $proto;
        $version_dir = ROOT_PATH."/protos/".$proto->id."/versions";
        if(is_dir($version_dir)){
            $dir_files = scandir($version_dir, SCANDIR_SORT_DESCENDING);
            foreach($dir_files as $file){
                if(pathinfo($version_dir."/".$file, PATHINFO_EXTENSION) == 'php'){
                    $version_name = explode('.', $file);
                    $version_files[] = $version_name[0];
                }
            }
        ?>
                <a href="<?php echo ROOT_URL ?>" class="label label-success">current</a>
            <?php foreach($version_files as $version): ?>
                <a href="<?php echo ROOT_URL."/".$proto->id."/".$version ?>" class="label label-default"><?php echo $version; ?></a>
            <?php endforeach; ?>
    <?php
        }
    }

    function random_hex($hashtag = '#'){
        $rand = array('0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f');
        $color = $hashtag.$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)].$rand[rand(0,15)];
        return $color;
    }

    function get_img($image, $dir = 'img'){
        return ROOT_URL."/img/".$image;
    }
    function get_src($media, $dir = 'medias'){
        global $proto;
        return ROOT_URL."/medias/".$media;
    }

function do_colors($it){
    $colors = array(
        0=>"CE6B5D",
        1=>"941F1F",
        2=>"FFEFB9",
        3=>"7B9971",
        4=>"34502B",
    );
    for ($i=0; $i < 5; $i++) {
        $colors = array_merge($colors, $colors);
    }
    return '#'.$colors[$it];
}

function allow_day($day){
    if(date('n') == 12 && date('j') >= $day){
        return true;
    }
    if($_GET['xyz'] >= $day){
        return true;
    }
    return false;
}

    function data_days(){
        global $proto;
        $days = array(
            '1'=>array(
                'id'=>1,
                'display'=>'window',
                'type'=>'animated-gif',
                'color'=>do_colors(0),
            ),
            '2'=>array(
                'id'=>2,
                'display'=>'window',
                'type'=>'animated-gif',
                'color'=>do_colors(1),
            ),
            '3'=>array(
                'id'=>3,
                'display'=>'audio',
                'type'=>'audio',
                'src'=>ROOT_URL."/medias/petitchat.mp3",
                'color'=>do_colors(2),
            ),
            '4'=>array(
                'id'=>4,
                'display'=>'window',
                'type'=>'animated-gif',
                'color'=>do_colors(3),
            ),
            '5'=>array(
                'id'=>5,
                'display'=>'window',
                'type'=>'video',
                'color'=>do_colors(4),
            ),
            '6'=>array(
                'id'=>6,
                'display'=>'window',
                'type'=>'video',
                'color'=>do_colors(5),
            ),
            '7'=>array(
                'id'=>7,
                'display'=>'window',
                'type'=>'video',
                'color'=>do_colors(6),
            ),
            '8'=>array(
                'id'=>8,
                'display'=>'window',
                'type'=>'animated-gif',
                'color'=>do_colors(7),
            ),
            '9'=>array(
                'id'=>9,
                'display'=>'window',
                'type'=>'video',
                'color'=>do_colors(8),
            ),
            '10'=>array(
                'id'=>10,
                'display'=>'window',
                'type'=>'video',
                'color'=>do_colors(9),
            ),
            '11'=>array(
                'id'=>11,
                'display'=>'window',
                'type'=>'video',
                'color'=>do_colors(10),
            ),
            '12'=>array(
                'id'=>12,
                'display'=>'audio',
                'type'=>'audio',
                'src'=>ROOT_URL."/medias/on-peut-rien-faire.mp3",
                'color'=>do_colors(11),
            ),
            '13'=>array(
                'id'=>13,
                'display'=>'window',
                'type'=>'animated-gif',
                'color'=>do_colors(12),
            ),
            '14'=>array(
                'id'=>14,
                'display'=>'window',
                'type'=>'video',
                'color'=>do_colors(13),
            ),
            '15'=>array(
                'id'=>15,
                'display'=>'window',
                'type'=>'video',
                'color'=>do_colors(14),
            ),
            '16'=>array(
                'id'=>16,
                'display'=>'audio',
                'type'=>'audio',
                'src'=>ROOT_URL."/medias/chauve-souris.mp3",
                'color'=>do_colors(15),
            ),
            '17'=>array(
                'id'=>17,
                'display'=>'window',
                'type'=>'animated-gif',
                'color'=>do_colors(16),
            ),
            '18'=>array(
                'id'=>18,
                'display'=>'window',
                'type'=>'video',
                'color'=>do_colors(17),
            ),
            '19'=>array(
                'id'=>19,
                'display'=>'window',
                'type'=>'video',
                'color'=>do_colors(18),
            ),
            '20'=>array(
                'id'=>20,
                'display'=>'window',
                'type'=>'video',
                'color'=>do_colors(19),
            ),
            '21'=>array(
                'id'=>21,
                'display'=>'window',
                'type'=>'animated-gif',
                'color'=>do_colors(20),
            ),
            '22'=>array(
                'id'=>22,
                'display'=>'window',
                'type'=>'video',
                'color'=>do_colors(21),
            ),
            '23'=>array(
                'id'=>23,
                'display'=>'audio',
                'type'=>'audio',
                'src'=>ROOT_URL."/medias/equipe.mp3",
                'color'=>do_colors(22),
            ),
            '24'=>array(
                'id'=>24,
                'display'=>'window',
                'type'=>'video',
                'color'=>do_colors(23),
            ),
/*            '25'=>array(
                'id'=>25,
                'display'=>'window',
                'type'=>'video',
                'color'=>do_colors(24),
            ),*/
        );
    if(count($days) < 24){
        for ($i= count($days) + 1; $i <= 25 ; $i++) {
            $complete[$i] = array('id'=>$i, 'color'=>do_colors($i-1));
        }
        $output = array_merge($days, $complete);
    }
    else{
        $output = $days;
    }
    return $output;
}
?>