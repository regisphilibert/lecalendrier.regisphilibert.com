<?php
define(ROOT_URL, "http://$_SERVER[HTTP_HOST]");
define(ROOT_PATH, $_SERVER["DOCUMENT_ROOT"]);
require "functions.php";
if(isset($_GET['day']) && !empty($_GET['day'])){
    include "days/".$_GET['day'].".php";
    die;
}
?>
<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title></title>
        <meta name="description" content="Le Calendrier de l'Avent d'Arthur et Alexandre">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <script src="<?php echo ROOT_URL ?>/js/head.script-min.js"></script>
        <link href='http://fonts.googleapis.com/css?family=Mountains+of+Christmas:400,700' rel='stylesheet' type='text/css'>
        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->
        <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet">
        <link rel="stylesheet" href="<?php echo ROOT_URL ?>/style.css">
        <script>
            var protoPath = '<?php echo ROOT_URL."/"; ?>';
        </script>
        <script type="text/javascript">

            var _gaq = _gaq || [];
            _gaq.push(['_setAccount', 'UA-57392990-1']);

            (function() {
                var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
                ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
                var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
            })();

        </script>
    </head>
    <body>
        <!--[if lt IE 7]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <!-- Add your site or application content here -->

        <?php include "includes/header.php"; ?>
        <div class="container">
            <?php include "includes/calendar.php"; ?>
        </div>
        <footer>
            <div class="container">
                 <div class="footer-content">
                    <div class="half-and-half">
                        <span class="half">Régis et Mariane (Tonton, Tata)</span>
                        <span class="half">
                            <img src="<?php echo get_img('regis.png') ?>" alt=""><img src="<?php echo get_img('mariane.png'); ?>" alt="">
                        </span>
                    </div>
                </div>
            </div>
        </footer>
        <script src="<?php echo ROOT_URL ?>/js/avcal.script-min.js"></script>
    </body>
</html>
