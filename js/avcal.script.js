(function( $ ){
   $.fn.calDestroy = function() {
    that = jQuery.data(this, 'origin-window', that);
    returnStyle = {
        'width':that.width(),
        'height':that.height(),
        'left':that.offset().left - $(window).scrollLeft(),
        'top':that.offset().top - $(window).scrollTop(),
    };
    if(that.attr('data-type') == 'text' || that.attr('data-type') == 'video'){
        this.removeClass('loaded');
    }
    this.animate(returnStyle, 500, function(){
        jQuery.data(this, 'origin-window', that).removeClass('opened');
        calWindow.remove();
    });
   };
})( jQuery );

$(document).ready(function(){
/*    var audioBing = document.createElement('audio');
    audioBing.setAttribute('src', protoPath+'audio/coin.wav');*/
    $('.square-widget[data-display!="none"]').click(function(){
        that = $(this);
        if(that.attr('data-display') == 'window'){
            that.addClass('opened');
            calWindow = $('<div class="cal-window">');
            jQuery.data(calWindow, 'origin-window', that);
            jQuery.data(calWindow, 'origin-style',
                {
                    'width':that.width(),
                    'height':that.height(),
                    'left':that.offset().left - $(window).scrollLeft(),
                    'top':that.offset().top - $(window).scrollTop(),
                    'background-color':that.css('background-color'),
                }
            );
            calWindow.css(jQuery.data(calWindow, 'origin-style'));
            thisDay = that.attr('data-day');

            $.ajax({
                url: protoPath+"/?day="+thisDay,
            }).done(function(data) {
                calWindow.html(data);
                if(calWindow.find('video').length){
                    calVideoElem = calWindow.find('video');
                    calVideo = calVideoElem.get(0);
                    calVideo.addEventListener('ended', function () {
                        calWindow.calDestroy();
                    });
                    calVideo.addEventListener('webkitendfullscreen', function () {
                        calWindow.calDestroy();
                    });
                    if(!Modernizr.touch){
                        calVideo.play();
                    }

                    calVideo.addEventListener('play', function () {
                        calWindow.addClass('playing');
                    });
                }
            });
            calWindow.appendTo('body').animate({
                'width':'80%',
                'height':'80%',
                'left':'10%',
                'top':'10%'
            }, 500, function(){
                calWindow.addClass('loaded');
            });
        }
        else if(that.attr('data-display') == 'audio' && !that.hasClass('listening')){
            calAudio = document.createElement('audio');
            calAudio.setAttribute('src', that.attr('data-src'));
            calAudio.addEventListener('loadstart', function () {
                that.addClass('listening playing');
            });
            calAudio.addEventListener('ended', function () {
                that.removeClass('listening playing');
            });
            calAudio.play();
        }
        else if(that.attr('data-display') == 'audio' && that.hasClass('listening')){
            if(calAudio.paused){
                calAudio.play();
                that.addClass('playing');
            }
            else{
                calAudio.pause();
                that.removeClass('playing');
            }
        }
        _gaq.push(['_trackPageview', '/day/'+that.attr('data-day')]);
    });
    $('.no-touch body').on('click', '.window-closer', function(){
        calWindow.calDestroy();
    });
    $('.touch body').on('click touchstart', '.window-closer', function(){
        if($('.window-closer').hasClass('video')){
            if($('.window-closer').parent('.cal-window').hasClass('playing')){
                calWindow.calDestroy();
            }
        }
        else{
            calWindow.calDestroy();
        }
    });
});